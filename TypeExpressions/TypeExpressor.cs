﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace TypeExpressions
{
    public abstract class TypeExpressor
    {
        private static readonly Hashtable expressors = new Hashtable();
        private static readonly ParameterExpression instanceExpression = Expression.Parameter(typeof(object), "instance");
        private static readonly ParameterExpression parameterExpression = Expression.Parameter(typeof(object[]), "args");
        private const string ParamSeparator = "|";

        public const BindingFlags Default = BindingFlags.Public | BindingFlags.Instance;
        public const BindingFlags Private = Default | BindingFlags.NonPublic;
        public const BindingFlags Static = BindingFlags.Public | BindingFlags.Static;
        public const BindingFlags PrivateStatic = Static | BindingFlags.NonPublic;

        public abstract object this[object target, string name] { get; set; }
        public abstract Type Type { get; }
        public abstract BindingFlags Flags { get; }
        public bool IsCOMType
        {
            get { return Type.IsCOMObject; }
        }

        public static TypeExpressor Create(Type type)
        {
            return Create(type, Static);
        }
        public static TypeExpressor Create(Type type, BindingFlags flags)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            TypeExpressor tExp = (TypeExpressor)expressors[type];
            if (tExp != null)
                return tExp;

            lock (expressors)
            {
                tExp = (TypeExpressor)expressors[type];
                if (tExp != null)
                    return tExp;

                tExp = CreateNew(type, flags);

                expressors[type] = tExp;
                return tExp;
            }
        }
        static TypeExpressor CreateNew(Type type, BindingFlags flags)
        {
            var ctors = type.GetConstructors(flags);
            var props = type.GetProperties(flags);
            var fields = type.GetFields(flags);
            var methods = type.GetMethods(flags).Where(m => !m.IsSpecialName).ToArray();

            var ctorMap = new Dictionary<string, CompiledActivator>(ctors.Length);
            var fieldMap = new Dictionary<string, CompiledMember>(fields.Length);
            var getterMap = new Dictionary<string, CompiledFunction>(props.Length);
            var setterMap = new Dictionary<string, CompiledAction>(props.Length);
            var methodMap = new Dictionary<string, Delegate>(methods.Length);

            foreach (ConstructorInfo ci in ctors)
            {
                var ctor = GetCompiledActivator(ci);
                if (ctor != null)
                {
                    string signature = GetMethodSignature("ctor", ci.GetParameters());
                    ctorMap.Add(signature, ctor);
                }
            }

            foreach (PropertyInfo pi in props)
            {
                var getter = (CompiledFunction)GetCompiledMethod(pi.GetGetMethod(true));
                var setter = (CompiledAction)GetCompiledMethod(pi.GetSetMethod(true));

                if (getter != null && !getterMap.ContainsKey(pi.Name))
                {
                    getterMap.Add(pi.Name, getter);
                }

                if (setter != null && !setterMap.ContainsKey(pi.Name))
                {
                    setterMap.Add(pi.Name, setter);
                }
            }

            foreach (FieldInfo fi in fields)
            {
                var field = GetCompiledMember(fi);
                if (field != null)
                    fieldMap.Add(fi.Name, field);
            }

            foreach (MethodInfo mi in methods)
            {
                var method = GetCompiledMethod(mi);
                if (method != null)
                {
                    string signature = GetMethodSignature(mi);
                    methodMap.Add(signature, method);
                }
            }

            return new DelegateExpressor(type, flags, ctorMap, getterMap, setterMap, fieldMap, methodMap);
        }

        public abstract object Construct(params object[] args);
        public abstract object Get(object target, string memberName);
        public abstract void Set(object target, string memberName, object value);
        public abstract object Invoke(object target, string methodName, params object[] args);

        private static CompiledActivator GetCompiledActivator(ConstructorInfo constructorInfo)
        {
            if (constructorInfo == null)
                return null;

            var parameters = GetParameterExpressions(constructorInfo);
            var ctorExpression = Expression.New(constructorInfo, parameters.ToArray());
            var lambda = Expression.Lambda(typeof(CompiledActivator), ctorExpression, parameterExpression);
            var compiledExpression = (CompiledActivator)lambda.Compile();

            return compiledExpression;
        }
        private static CompiledMember GetCompiledMember(MemberInfo memberInfo)
        {
            if (memberInfo == null)
                return null;

            var convertedInstance = Expression.Convert(instanceExpression, memberInfo.DeclaringType);
            var memberExpression = Expression.MakeMemberAccess(convertedInstance, memberInfo);
            var convertedValue = Expression.Convert(memberExpression, typeof(object));
            var lambda = Expression.Lambda<CompiledMember>(convertedValue, instanceExpression);
            var compiledMember = lambda.Compile();

            return compiledMember;
        }
        private static Delegate GetCompiledMethod(MethodInfo methodInfo)
        {
            if (methodInfo == null)
                return null;

            var parameters = GetParameterExpressions(methodInfo).ToArray();
            var convertedInstance = Expression.Convert(instanceExpression, methodInfo.DeclaringType);
            var methodExpression = Expression.Call(convertedInstance, methodInfo, parameters.ToArray());

            if (methodInfo.ReturnType == typeof(void))
            {
                var lambda = Expression.Lambda<CompiledAction>(methodExpression, instanceExpression, parameterExpression);
                return lambda.Compile();
            }
            else
            {
                var convertedExpression = Expression.Convert(methodExpression, typeof(object));
                var lambda = Expression.Lambda<CompiledFunction>(convertedExpression, instanceExpression, parameterExpression);
                return lambda.Compile();
            }
        }
        private static IEnumerable<Expression> GetParameterExpressions(MethodBase method)
        {
            ParameterInfo[] methodParameters = method.GetParameters();
            for (int i = 0; i < methodParameters.Length; i++)
            {
                ParameterInfo param = methodParameters[i];
                Type t = param.ParameterType;

                var index = Expression.Constant(i);
                var item = Expression.ArrayIndex(parameterExpression, index);
                var converted = Expression.Convert(item, t.IsByRef ? t.GetElementType() : t);

                yield return converted;
            }
        }
        private static string GetMethodSignature(MethodInfo methodInfo)
        {
            return GetMethodSignature(methodInfo.Name, methodInfo.GetParameters());
        }
        private static string GetMethodSignature(string methodName, ParameterInfo[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
                return methodName;

            string name = methodName;
            foreach (ParameterInfo p in parameters)
            {
                Type t = p.ParameterType;
                name += String.Concat(ParamSeparator, t.IsByRef ? t.GetElementType().Name : t.Name);
            }

            return name;
        }
        private static string GetMethodSignature(string methodName, params object[] args)
        {
            if (args == null || args.Length == 0)
                return methodName;

            string name = methodName;
            foreach (object arg in args)
            {
                name += String.Concat(ParamSeparator, arg.GetType().Name);
            }

            return name;
        }

        sealed class DelegateExpressor : TypeExpressor
        {
            private readonly IDictionary<string, CompiledActivator> _activators;
            private readonly IDictionary<string, CompiledMember> _fields;
            private readonly IDictionary<string, CompiledFunction> _getters;
            private readonly IDictionary<string, CompiledAction> _setters;
            private readonly IDictionary<string, Delegate> _methods;
            private readonly Type _type;
            private readonly BindingFlags _flags;

            public override Type Type
            {
                get { return _type; }
            }
            public override BindingFlags Flags
            {
                get { return _flags; }
            }

            public DelegateExpressor(Type type, BindingFlags flags, IDictionary<string, CompiledActivator> activators,
                IDictionary<string, CompiledFunction> getters, IDictionary<string, CompiledAction> setters,
                IDictionary<string, CompiledMember> fields, IDictionary<string, Delegate> methods)
            {
                this._type = type;
                this._flags = flags;
                this._activators = activators;
                this._fields = fields;
                this._getters = getters;
                this._setters = setters;
                this._methods = methods;
            }

            public override object this[object target, string name]
            {
                get { return Get(target, name); }
                set { Set(target, name, value); }
            }

            public override object Construct(params object[] args)
            {
                string signature = GetMethodSignature("ctor", args);

                CompiledActivator ctor;
                if (!_activators.TryGetValue(signature, out ctor))
                    throw new MissingMethodException(this.Type.FullName, "ctor");

                return ctor(args);
            }
            public override object Get(object target, string memberName)
            {
                if (string.IsNullOrEmpty(memberName))
                    throw new ArgumentNullException("memberName");

                CompiledFunction getter;
                if (_getters.TryGetValue(memberName, out getter))
                    return getter(target);

                CompiledMember field;
                if (_fields.TryGetValue(memberName, out field))
                    return field(target);

                throw new MissingMemberException(this.Type.FullName, memberName);
            }
            public override void Set(object target, string memberName, object value)
            {
                if (string.IsNullOrEmpty(memberName))
                    throw new ArgumentNullException("memberName");

                CompiledAction setter;
                if (!_setters.TryGetValue(memberName, out setter))
                    throw new MissingMethodException(this.Type.FullName, memberName);

                ((CompiledAction)setter)(target, value);
            }
            public override object Invoke(object target, string methodName, params object[] args)
            {
                if (string.IsNullOrEmpty(methodName))
                    throw new ArgumentNullException("methodName");

                Delegate func;
                string signature = GetMethodSignature(methodName, args);
                if (!_methods.TryGetValue(signature, out func))
                {
                    if (IsCOMType)
                        return COMInvoke(target, methodName, args);

                    throw new MissingMethodException(this.Type.FullName, methodName);
                }


                // Check for void return
                if (func.Method.ReturnType == typeof(void))
                {
                    ((CompiledAction)func)(target, args);
                    return null;
                }

                return ((CompiledFunction)func)(target, args);
            }

            private object COMInvoke(object target, string methodName, object[] args)
            {
                try
                {
                    var invokeFlags = Flags | BindingFlags.InvokeMethod;
                    return target.GetType().InvokeMember(methodName, invokeFlags, default(Binder), target, args);
                }
                catch (Exception ex)
                {
                    throw new TargetInvocationException(ex);
                }
            }
        }
    }
}
