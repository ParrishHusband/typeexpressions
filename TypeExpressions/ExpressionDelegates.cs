﻿namespace TypeExpressions
{
    public delegate object CompiledActivator(params object[] args);
    public delegate void CompiledAction(object instance, params object[] args);
    public delegate object CompiledFunction(object instance, params object[] args);
    public delegate object CompiledMember(object instance);
}
