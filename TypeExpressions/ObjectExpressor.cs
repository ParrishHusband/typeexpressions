﻿using System;
using System.Reflection;

namespace TypeExpressions
{
    public abstract class ObjectExpressor
    {
        #region Properties

        public abstract object this[string name] { get; set; }
        public abstract object Target { get; }

        #endregion

        #region Methods

        public abstract object Invoke(string name, params object[] args);

        public static ObjectExpressor Create(object target)
        {
            return Create(target, TypeExpressor.Default);
        }
        public static ObjectExpressor Create(object target, BindingFlags flags)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            return new TypeExpressorWrapper(target, TypeExpressor.Create(target.GetType(), flags));
        }
        public static ObjectExpressor CreateNew(Type type, params object[] args)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            var expressor = TypeExpressor.Create(type, TypeExpressor.Default);
            var target = expressor.Construct(args);

            return new TypeExpressorWrapper(target, expressor);
        }

        public override bool Equals(object obj)
        {
            return Target.Equals(obj);
        }
        public override int GetHashCode()
        {
            return Target.GetHashCode();
        }
        public override string ToString()
        {
            return Target.ToString();
        }

        #endregion

        sealed class TypeExpressorWrapper : ObjectExpressor
        {
            private readonly object _target;
            private readonly TypeExpressor _expressor;

            public override object this[string name]
            {
                get { return _expressor.Get(_target, name); }
                set { _expressor.Set(_target, name, value); }
            }
            public override object Target
            {
                get { return _target; }
            }

            public TypeExpressorWrapper(object target, TypeExpressor expressor)
            {
                this._target = target;
                this._expressor = expressor;
            }

            public override object Invoke(string name, params object[] args)
            {
                return _expressor.Invoke(_target, name, args);
            }
        }
    }
}
