﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TypeExpressions
{
    public static class TypeExtensions
    {
        public static FieldInfo GetFieldOfType(this Type type, Type fieldType)
        {
            return GetFieldOfType(type, fieldType, TypeExpressor.Default);
        }
        public static FieldInfo GetFieldOfType(this Type type, Type fieldType, BindingFlags flags)
        {
            return type.GetFields(flags).FirstOrDefault(f => f.FieldType == fieldType);
        }
        public static PropertyInfo GetPropertyOfType(this Type type, Type propertyType)
        {
            return GetPropertyOfType(type, propertyType, TypeExpressor.Default);
        }
        public static PropertyInfo GetPropertyOfType(this Type type, Type propertyType, BindingFlags flags)
        {
            return type.GetProperties(flags).FirstOrDefault(p => p.PropertyType == propertyType);
        }
        public static TypeExpressor AsExpressor(this Type type)
        {
            return AsExpressor(type, TypeExpressor.Default);
        }
        public static TypeExpressor AsExpressor(this Type type, BindingFlags flags)
        {
            if (type == null)
                throw new TargetException();

            return TypeExpressor.Create(type, flags);
        }
        public static ObjectExpressor AsExpressor(this object target)
        {
            return AsExpressor(target, TypeExpressor.Default);
        }
        public static ObjectExpressor AsExpressor(this object target, BindingFlags flags)
        {
            if (target == null)
                throw new TargetException();

            return ObjectExpressor.Create(target, flags);
        }
    }
}
